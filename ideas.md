# Ideas:


* `r <shortcut>` - a shell function (like `rce`) that runs the most frecent rbe cmd exec command matching the shortcut.  Think `z` for `rce`
    * May have to do something about the built-in zsh `r` command `disable r` will disable it, at least for the session, so maybe have that in whatever file is sourced to get my `r` command if just creating a shell function doesn't work
* `{{@...}}` to expand a list-type var as separate args; using it on a non-list-type will have no special effect (a.k.a. it ignores the `@` if used with a non-list-type).
    * **GOAL:** turn the `echo {{var_name}} | xargs rbe c e <cmd_call>` commands into `<cmd_call> {{@var_name}}`
    * The `@` goes before the `#` in the pattern.
    * Because only permanent vars can be list-type, this will have no effect on temporary vars
        * This means that if the list-type var exists as a permanent var, the `#` will have no effect
        * If the var name starts with `_`, the `@` will have to be ignored, since `_` forces temporary var
        * This also means that if the var call has an indexed fallback (such as `{{...=>0}}`), the indexed fallback will be ignored if the var exists
        * Keep in mind that vars defined under commands (group/alias) are currently handled as temporary vars, so they don't support list type and thus will ignore the `@`
        * Even if a permanent version of the var exists at some level as a list-type, it doesn't count if that isn't the acting version (due to command-stored vars, local var overrides, or using the `--var` with the call to `rbe c e`)
            * In other words, all of this behavior is based on the **ACTING** version of the var (whatever would be used without the `@`)
    * Handle in subs_vars
    * Consider restrictions on use:
        * Potentially only allow use as the entire arg
        * Alternative is to allow the `**...**` var loop to support this, so that processing can be done on it.
            * If I go this route, I need to block indexed vars from being included in this var loop
    * Use restriction violations will result in ignoring the @, causing the list-type var to be substituted as a single space-joined string like happens now (and will still happen after this feature when the @ is left out)
* As an alternative to the above idea, it may be possible to do something like `--arg-var-append`
    * **GOAL:** turn the `echo {{var_name}} | xargs rbe c e <cmd_call>` commands into `<cmd_call>` with a special `arg_var_append` property
    * Would be a list of var names to add to extra_args
        * Likely at the beginning, so that it doesn't get put after user-supplied args to `rbe c e`
        * Current xargs-style commands would place the user-supplied `rbe c e` args after the list-style var expansion, so that's why I feel the behavior should be kept
            * This may not actually be the case; I don't think I've ever tried something like this, but i still would want the above behavior instead of whatever actually happens right now
    * Name of flag/property may change (depends on if I come up with a better name)
    * Much simpler than the above idea, but much less powerful
        * Maybe do this as a first version, then do the above idea at a later point in time
            * Maybe leave this in when adding the above idea
                * Would be good for maintaining backwards compatibility with older command definitions
* Shorthand args
    * **GOAL:** turn the `gco-t` and similar into `gco t` where `t` is mapped under a `branches` shorthand group and is expanded when provided as an arg to any command that ties itself to the `branches` shorthand group
    * A single command can have multiple connected shorthand groups
    * Will need to create `shorthands.rbe.yaml`, and allow local shorthands
    * Will need to create `shorthand` top-level rbe sub-command
        * This subcommand should handle the creation and management of the shorthands
        * Will be able to add and remove entries from a shorthand mapping by shorthand (which is what I call both the feature and the key of the maps, but in this case I mean the key of the maps)
        * Will have a `rbe shorthand create` (aliased to `init`) command that will create a new empty shorthand group
        * Will have a `rbe shorthand add` (aliased to `put`) command that takes the first parameter of the shorthand group name, second parameter of the shorthand string (no spaces allowed), and third parameter of the expansion
            * If the group specified does not exist, confirm that the user wants to create the group, unless the `--auto-create` (aliased to `--auto-init`) flag is passed
            * If the shorthand is already mapped in that group, confirm that the user wants to overwrite the existing expansion, unless the `--auto-update` (aliased to `--auto-set`) flag is passed
        * Will have a `rbe shorthand update` (aliased to `set`) command that will update an existing shorthand mapping.  First parameter is group name, second is shorthand string (no spaces allowed), and third is the new expansion
            * If the group specified does not exist, confirm that the user wants to create the group, unless the `--auto-create` (aliased to `--auto-init`) flag is passed
                * If the group does not already exist and either the user confirms the creation or passes one of those group creation flags, do not confirm adding a new entry
            * If the shorthand is not yet mapped in that group, confirm that the user wants to create a new mapping, unless the `--auto-add` (aliased to `--auto-put`) flag is passed
        * Will have a `rbe shorthand bulk-add` (aliased to `bulk-put`, `multi-add`, and `multi-put`) command to add multiple entries at once.  First parameter is the group name, followed by a varargs of `key:value` pairs
            * Flags and exist/non-exist behavior copied from `add`, but with these differences:
                * If `--auto-update` flag is specified, do not confirm on any of the overwrites
                * When `--auto-update` is not passed, confirm each overwrite, but give 5 options, `[Y]es`, `[N]o`, `[A]ll`, `N[o]ne`, and `[S]ome`.  Yes/no will continue asking each time, but once we get All/None, we apply that answer to the remainder of the prompts (but not the ones already answered).  "Some" will use checkboxes from [inquirer.rb](https://github.com/arlimus/inquirer.rb) to get which ones
        * Will have a `rbe shorthand bulk-update` (aliased to `bulk-set`, `multi-update`, and `multi-set`) command to add multiple entries at once.  Parameters match `bulk-add`
            * Flags and exist/non-exist behavior copied from `update`, but with these differences:
                * Differences copied from `bulk-add`, but with tweaks to match the differences between `add` and `update`
        * Will have a `rbe shorthand delete` (aliased to `remove`) command to remove **individual entries** from a shorthand group.  The first parameter is the group name, followed by a varargs of the shorthands to drop
            * Print out a message and exit if the group doesn't exist
            * Print out a list of the mappings to delete, along with a list of the ones that don't exist (if any), and prompt the user for confirmation if the `--auto-confirm` flag is not passed
                * If there is only one entry to delete, use confirmation options `[Y]es` and `[N]o`
                * If there are multiple to delete, use options `[A]ll`, `N[o]ne`, and `[S]ome`, with handling similar to how `bulk-add` handles these options
            * If the user removes all entries from a group, ask if they want to remove the group, unless the `--auto-drop` (aliased to `--auto-clean`) flag is specified, in which case it is automatically confirmed
        * Will have a `rbe shorthand drop` (no alias currently in mind) command to remove one or more shorthand groups.  The only parameter is a varargs of the list of groups to remove
            * Print out a list of the groups to delete (with the mappings they contain, unless the `--quiet` flag is given), along with a list of the ones that don't exist (if any), and prompt the user for confirmation if the `--auto-confirm` flag is not passed
                * Confirmation prompting is handled the same as `delete`
        * Will have a `rbe shorthand sort` command to sort the list of groups
        * Will have a `rbe shorthand group-sort` (aliased to `shorthand-sort`) command to sort the entries within a group
        * Will have a `rbe shorthand interactive` command to go through an interactive loop using the `inquirer.rb` library to handle all operations listed above.
            * Will use bulk handling (with checkboxes) for anything modifying existing data (so not `add` or `create`)
                * Will prompt for each value, with a blank response or ctrl-c meaning cancel (don't actually exit on ctrl-c)
            * Ctrl-c will always go up a level, unless we are at the top level, where it will exit
            * Any list selection (not checkbox) will have a "Back" or "Cancel" option (except the top level one) to do the same as ctrl-c in that spot
            * Any list selection (not checkbox) will have an "Exit" option to exit out of the entire interactive loop
            * May come up with more later
