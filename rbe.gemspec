# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rbe/version'

Gem::Specification.new do |spec|
  spec.name        = 'rbe'
  spec.version     = Rbe::VERSION
  spec.authors     = ['Eric Henderson']
  spec.email       = ['henderea@gmail.com']
  spec.summary     = %q{RuBy Executor}
  spec.description = %q{A tool for making it easier to run common commands.  Supports storing and automatically filling in your sudo password. Also supports saving common commands and calling them by name.  Groups of commands can also be saved and called by name.  Commands can take parameters and plug them into specific points in the command or just append them on the end.  Also supports a variable system that allows local and global variables, temporary variables, and required variables (which will ask for the value if it isn't already known).}
  spec.homepage    = 'https://gitlab.com/henderea/rbe'
  spec.license     = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.required_ruby_version = '>= 1.9.3'

  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'rake', '~> 12.3'

  spec.add_dependency 'everyday_thor_util', '~> 2.0', '>= 2.0.8'
  spec.add_dependency 'everyday-cli-utils', '~> 1.8', '>= 1.8.7.2'
  spec.add_dependency 'thor', '~> 0.20', '>= 0.20.3'
  spec.add_dependency 'ruby-keychain', '~> 0.3', '>= 0.3.2'
  spec.add_dependency 'everyday_natsort', '~> 1.0', '>= 1.0.4'
end
