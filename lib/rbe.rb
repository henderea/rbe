require 'rbe/version'

def load_ds(*names)
  @ds_loaded ||= []
  names.each { |name|
    unless @ds_loaded.include?(name)
      require "rbe/data/#{name}"
      @ds_loaded << name
    end
  }
  Rbe::Data::DataStore
end

tl = ENV['time_load']
@time_load = tl == '1' || tl == 1 || tl == 'true' || tl == 't'

module Rbe
end
@lt = []
@group = []
def lt_gs(name)
  @group.unshift name
end
def lt_ge
  @group.shift
  lt(nil)
end
def lt(name)
  @lt << { name: name, group: @group.clone, time: Time.now }
end
lt_gs 'require time'
lt 'thor'
require 'thor'
lt 'etu/builder'
require 'everyday_thor_util/builder'
lt 'include ETU::Builder'
include EverydayThorUtil::Builder
lt 'etu/thor-print-fix'
require 'everyday_thor_util/thor-print-fix'
lt 'everyday-cli-utils'
require 'everyday-cli-utils'
lt 'include EverydayCliUtils'
include EverydayCliUtils
lt 'import :format'
import :format
lt_gs 'rbe/data/data_store'
require 'rbe/data/data_store'
lt_ge
lt_gs 'rbe/cli/helpers'
require 'rbe/cli/helpers'
lt_ge
lt 'rbe/cli/commands'
require 'rbe/cli/commands'
lt_ge

lt_gs 'build + add debugging time'
lt 'build time'
@rc = build!

lt 'add debugging time'
add_debugging(@rc, nil, 'debug')

lt_ge
lt(nil)

def ct(a, b)
  (a-b)*1000
end

def format_time(d, pd = nil)
  "#{('%.3f' % d).rjust(10)} ms#{pd.nil? ? '' : " (#{'%.3f' % ((d / pd) * 100.0)}%)"}"
end

def calc_display(node, parent_time_diff = nil, level = 0)
  text = "#{level > 0 ? "#{' ' * (4*(level-1)+1)}-- " : ''}#{node[:name] || 'total time'}:"
  dt = node[:time_diff]
  ty = node[:type]
  time = format_time(dt, parent_time_diff)
  disp = [{ text: text, time: time }]
  ch = node[:children]
  unless ch.nil? || ch.empty?
    ch.each { |c|
      disp += calc_display(c, dt, level + (ty == :total ? 0 : 1))
    }
  end
  disp
end

def calc_times
  last_name = nil
  last_group = []
  last_time = nil
  start_time = []
  processed = [[]]
  completed = nil
  @lt.each { |t|
    n = t[:name]
    g = t[:group]
    ti = t[:time]
    if last_time.nil?
      start_time << ti
    elsif !last_name.nil?
      dt = ct(ti,last_time)
      processed[0] << { type: :item, name: last_name, time_diff: dt, children: nil }
    end

    if n.nil?
      st = start_time.shift
      dt = ct(ti,st)
      pg = processed.shift
      if processed.count > 0
        processed[0] << { type: :group, name: last_group[0], time_diff: dt, children: pg }
      else
        completed = { type: :total, name: nil, time_diff: dt, children: pg }
        break
      end
    elsif g != last_group
      (g.count - last_group.count).times {
        processed.unshift []
        start_time.unshift ti
      }
    end

    last_name = n
    last_group = g
    last_time = ti
  }
  completed
end

if @time_load
  disp = calc_display(calc_times)
  ml = disp.map { |d| d[:text].length }.max
  mlt = disp.map { |d| d[:time].length }.max
  strs = disp.map { |d| "| #{d[:text].ljust(ml)} #{d[:time].ljust(mlt)} |" }
  strs.unshift("+#{'-' * (ml + mlt + 3)}+")
  strs << "+#{'-' * (ml + mlt + 3)}+"
  strs.each { |s| puts s }
  puts
  puts
end