require 'everyday_natsort'

root_command[:var] = command(aliases: %w(variable v), short_desc: 'var SUBCOMMAND ARGS...', desc: 'configure stored variables')

root_command[:var][:add] = command(short_desc: 'add VAR_NAME DEFAULT_VALUE', desc: 'add/modify a variable default value') { |name, value|
  Rbe::Data::DataStore.vars.save_local = options[:local]
  Rbe::Data::DataStore.vars[name]      = value
  puts "#{name} added".format_fg_green
}

root_command[:var][:add][:local] = flag(type: :boolean, desc: 'add/modify local variables')

root_command[:var][:list_add] = command(short_desc: 'list-add VAR_NAME DEFAULT_VALUES...', desc: 'add/modify a variable default value list') { |name, *values|
  Rbe::Data::DataStore.vars.save_local = options[:local]
  Rbe::Data::DataStore.vars[name]      = values
  puts "#{name} added".format_fg_green
}

root_command[:var][:list_add][:local] = flag(type: :boolean, desc: 'add/modify local variable value lists')

root_command[:var][:list_sort] = command(short_desc: 'list-sort VAR_NAME', desc: 'sort a variable default value list') { |name|
  Rbe::Data::DataStore.vars.save_local   = options[:local]
  Rbe::Data::DataStore.vars.search_local = options[:local]
  arr                                    = Rbe::Data::DataStore.vars[name]
  Rbe::Data::DataStore.vars[name]        = EverydayNatsort.sort(arr) if arr.is_a?(Array)
  puts "#{name} sorted".format_fg_green
}

root_command[:var][:list_sort][:local] = flag(type: :boolean, desc: 'sort local variable value lists')

root_command[:var][:var_sort] = command(short_desc: 'var-sort', desc: 'sort the variables in the vars.rbe.json file') {
  Rbe::Data::DataStore.vars.save_local   = options[:local]
  Rbe::Data::DataStore.vars.search_local = options[:local]
  Rbe::Data::DataStore.vars.sort_list
  puts 'Vars sorted'.format_fg_green
}

root_command[:var][:var_sort][:local] = flag(type: :boolean, desc: 'sort the variables in the local vars.rbe.json file')

root_command[:var][:list] = command(aliases: %w(ls), short_desc: 'list [var_name]', desc: 'list the variables with defaults, optionally filtering by variable name') { |var_name = nil|
  vars = Rbe::Data::DataStore.vars.keys
  vars = vars.grep(/.*#{var_name}.*/) if var_name
  vars.sort!
  if vars.nil? || vars.empty?
    puts "Did not find any variables matching #{var_name}".format_fg_yellow
  else
    longest_var = vars.map { |v| v.to_s.length }.max
    vars.each { |v| puts "#{v.to_s.ljust(longest_var)} => #{Rbe::Data::DataStore.vars[v].to_s}" }
  end
}

root_command[:var][:remove] = command(aliases: %w(rm delete), short_desc: 'remove var_name', desc: 'remove a variable default value') { |var_name|
  Rbe::Data::DataStore.vars.save_local = options[:local]
  if Rbe::Data::DataStore.vars.has_key?(var_name)
    Rbe::Data::DataStore.vars.delete(var_name)
    puts "#{var_name} deleted".format_fg_green
  else
    puts "#{var_name} not found".format_fg_yellow
  end
}

root_command[:var][:remove][:local] = flag(type: :boolean, desc: 'remove local variables')

root_command[:var][:rewrite] = command(aliases: %w(rw), short_desc: 'rewrite', desc: 'rewrite the variable storage files to fix formatting differences caused by manual editing') {
  Rbe::Data::DataStore.vars.write_vars
  puts 'Vars rewritten'.format_fg_green
}