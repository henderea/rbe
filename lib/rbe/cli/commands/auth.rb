root_command[:auth] = command(short_desc: 'auth', desc: 'authenticate and save password in keychain') {
  pw     = getpass
  result = Rbe::Data::DataStore.password.set(pw)
  puts result == :success ? 'Success!'.format_fg_green : 'Failure'.format_fg_red
}

root_command[:test_auth] = command(short_desc: 'test-auth', desc: 'test stored password') {
  pw = Rbe::Data::DataStore.password.get
  if pw.nil?
    puts 'You need to call `rbe auth` first!'.format_fg_yellow
  else
    puts testpass(pw) ? 'Success!'.format_fg_green : 'Failure'.format_fg_red
  end
}

root_command[:unauth] = command(short_desc: 'unauth', desc: 'remove stored password from keychain') {
  result = Rbe::Data::DataStore.password.delete
  if result == :nil
    puts 'No stored password!'.format_fg_yellow
  elsif result == :success
    puts 'Password forgotten'.format_fg_green
  else
    puts 'Failure to forget password'.format_fg_red
  end
}