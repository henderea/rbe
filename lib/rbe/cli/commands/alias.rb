root_command[:alias] = command(short_desc: 'alias SUBCOMMAND ARGS...', desc: 'configure command bash alias functions')

root_command[:alias][:add] = command(short_desc: 'add cmd_id...', desc: 'register an alias') { |*cmd_ids|
  changed = false
  cmd_ids.each { |cmd_id|
    if Rbe::Data::DataStore.command(cmd_id).nil?
      puts "Command with id #{cmd_id} does not seem to exist.".format_fg_yellow
    else
      Rbe::Data::DataStore.aliases << cmd_id.to_s
      puts "#{cmd_id} alias added".format_fg_green
      changed = true
    end
    exit 11 if changed
  }
}

root_command[:alias][:remove] = command(aliases: %w(rm delete), short_desc: 'remove cmd_id...', desc: 'remove an alias for a command') { |*cmd_ids|
  changed = false
  cmd_ids.each { |cmd_id|
    if Rbe::Data::DataStore.aliases.include?(cmd_id)
      Rbe::Data::DataStore.aliases.delete(cmd_id)
      puts "#{cmd_id} alias deleted".format_fg_green
      changed = true
    else
      puts "#{cmd_id} not aliased".format_fg_yellow
    end
  }
  exit 11 if changed
}

root_command[:alias][:list] = command(aliases: %w(ls), short_desc: 'list', desc: 'list the commands with aliases') {
  puts Rbe::Data::DataStore.aliases.list.join("\n")
}

root_command[:alias][:sort] = command(short_desc: 'sort', desc: 'sort the aliases in the aliases.rbe.json file') {
  Rbe::Data::DataStore.aliases.sort_list
  puts 'Aliases sorted'.format_fg_green
}

root_command[:alias][:update] = command(aliases: %w(up), short_desc: 'update [prefix="rvm reload 2>&- >&-"]', desc: 'update the bash files for the aliases that are registered with an optional prefix command') { |prefix = 'rvm reload 2>&- >&-'|
  update_rbe_reload(prefix)
  update_rbe_aliases(prefix)
  update_bashrc
  update_zshrc
}

root_command[:reload] = command(short_desc: 'reload', desc: 'reload rbe shell integration') {
  puts 'Please run "rbe alias update" to enable this command. It uses a shell function created by the alias system.'.format_fg_red
}