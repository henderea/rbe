root_command[:interactive] = command(aliases: %w(i), short_desc: 'interactive SHELL COMMAND ARGS...', desc: 'run a command interactively in the given shell') { |shell, cmd, *args|
  if cmd == 'sudo'
    sudo = 'sudo'
    cmd  = args.shift
  elsif cmd == 'rvmsudo'
    sudo = 'rvmsudo'
    cmd  = args.shift
  else
    sudo = nil
  end
  start, part = count_ind_vars(cmd, args)
  cmd_with_vars = subs_vars([cmd], [], true).first.first
  cmd_to_use, cmd_args = subs_command(cmd_with_vars, args)
  print_cmd(sudo, cmd_to_use, cmd_args, [], shell)
  run_cmd(sudo, shell, cmd_to_use, cmd_args, [])
}

root_command[:exec] = command(aliases: %w(e r), short_desc: 'exec COMMAND ARGS...', desc: 'run a shell command in a subshell') { |cmd, *args|
  if cmd == 'sudo'
    sudo = 'sudo'
    cmd  = args.shift
  elsif cmd == 'rvmsudo'
    sudo = 'rvmsudo'
    cmd  = args.shift
  else
    sudo = nil
  end
  start, part = count_ind_vars(cmd, args)
  cmd_with_vars = subs_vars([cmd], [], true).first.first
  cmd_to_use, cmd_args = subs_command(cmd_with_vars, args)
  print_cmd(sudo, cmd_to_use, cmd_args, [], nil)
  run_cmd(sudo, nil, cmd_to_use, cmd_args, [])
}