require 'everyday_natsort'

root_command[:sub] = command(aliases: %w(subs), short_desc: 'sub SUBCOMMAND ARGS...', desc: 'configure stored substitutions')

root_command[:sub][:add] = command(short_desc: 'add SUB_NAME MAPPED_VALUE', desc: 'add/modify a substitution mapping') { |name, value|
  Rbe::Data::DataStore.subs[name] = value
  puts "#{name} added".format_fg_green
}

root_command[:sub][:list_add] = command(short_desc: 'list-add SUB_NAME SUB_VALUES...', desc: 'add/modify a substitution list mapping') { |name, *values|
  Rbe::Data::DataStore.subs[name]      = values
  puts "#{name} added".format_fg_green
}

root_command[:sub][:list_sort] = command(short_desc: 'list-sort SUB_NAME', desc: 'sort a substitution list mapping') { |name|
  arr                                    = Rbe::Data::DataStore.subs[name]
  Rbe::Data::DataStore.subs[name]        = EverydayNatsort.sort(arr) if arr.is_a?(Array)
  puts "#{name} sorted".format_fg_green
}

root_command[:sub][:sub_sort] = command(short_desc: 'sub-sort', desc: 'sort the substitution mappings in the subs.rbe.json file') {
  Rbe::Data::DataStore.subs.sort_list
  puts 'Subs sorted'.format_fg_green
}

root_command[:sub][:list] = command(aliases: %w(ls), short_desc: 'list [sub_name]', desc: 'list the substitution mappings, optionally filtering by substitution name') { |sub_name = nil|
  subs = Rbe::Data::DataStore.subs.keys
  subs = subs.grep(/.*#{sub_name}.*/) if sub_name
  subs.sort!
  if subs.nil? || subs.empty?
    puts "Did not find any substitutions matching #{sub_name}".format_fg_yellow
  else
    longest_sub = subs.map { |v| v.to_s.length }.max
    subs.each { |v| puts "#{v.to_s.ljust(longest_sub)} => #{Rbe::Data::DataStore.subs[v].to_s}" }
  end
}

root_command[:sub][:remove] = command(aliases: %w(rm delete), short_desc: 'remove sub_name', desc: 'remove a substitution mapping') { |sub_name|
  if Rbe::Data::DataStore.subs.has_key?(sub_name)
    Rbe::Data::DataStore.subs.delete(sub_name)
    puts "#{sub_name} deleted".format_fg_green
  else
    puts "#{sub_name} not found".format_fg_yellow
  end
}

root_command[:sub][:rewrite] = command(aliases: %w(rw), short_desc: 'rewrite', desc: 'rewrite the substitution mapping storage file to fix formatting differences caused by manual editing') {
  Rbe::Data::DataStore.subs.write_subs
  puts 'Subs rewritten'.format_fg_green
}