require 'io/console'

global.helpers[:testpass] =->(pw = nil) {
  if load_keychain
    test = pw.nil? ? `sudo echo "Success" 2>&1`.chomp : `echo "#{pw}" | sudo -S echo "Success" 2>&1`.chomp
    test.include?('Success')
  else
    true
  end
}

global.helpers[:getpass] =->() {
  if load_keychain
    user = Rbe::Data::DataStore.user
    begin
      print "Password for user #{user}: "
      pw = STDIN.noecho(&:gets)
      puts
    end until testpass(pw)
    pw
  else
    false
  end
}

global.helpers[:run_sudo] =->(sudo_command, cmd, cmd_args, args, interactive = nil) {
  arr = array_to_args(cmd_args, args)
  if load_keychain
    pw  = Rbe::Data::DataStore.password.get
    if pw.nil?
      pw = getpass
    else
      unless testpass(pw)
        puts 'Stored password invalid!'.format_fg_red
        pw = getpass
      end
    end
    sudo_command = 'sudo' unless sudo_command == 'rvmsudo' && `which rvmsudo`.chomp.include?('rvmsudo')
    system(testpass ? "#{sudo_command} #{cmd} #{arr.join(' ')}" : "echo '#{pw}' | #{sudo_command} -S #{make_interactive(cmd, arr, interactive)}")
  else
    sudo_command = 'sudo' unless sudo_command == 'rvmsudo' && `which rvmsudo`.chomp.include?('rvmsudo')
    system("#{sudo_command} #{make_interactive(cmd, arr, interactive)}")
  end
}