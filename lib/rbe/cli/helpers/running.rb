class StateStore
  class << self
    attr_accessor :looping
  end
end

global.helpers[:run_cmd] =->(sudo_command, interactive, cmd, cmd_args, args) {
  arr = array_to_args(cmd_args, args, true)
  if sudo_command.nil?
    system(make_interactive(cmd, arr, interactive))
  else
    run_sudo(sudo_command, cmd, cmd_args, args, interactive)
  end
}

global.helpers[:exec_cmd] =->(cmd_id, *extra_args) {
  register_temp_vars(options[:var])
  cmd_id = subs_vars([cmd_id], extra_args, true).first.first
  cmd    = Rbe::Data::DataStore.command(cmd_id)
  if cmd
    register_temp_vars(cmd.vars)
    if cmd.command.is_a?(Array)
      puts "> #{cmd.sudo.nil? ? '' : "#{cmd.sudo} "}rbe cmd group-exec #{clean_cmd(cmd_id.to_s)} #{array_to_args((cmd.args || []), extra_args).join(' ')}" unless cmd.silent
      cmd.command.each { |c| exec_cmd(build_command_name(c, cmd.sudo, cmd.silent), *(cmd.args ? subs_vars(cmd.args, extra_args).first : extract_args(c, *extra_args))) }
    else
      start, part = count_ind_vars(cmd.command, cmd.args)
      if cmd.should_loop  && !StateStore.looping
        if (part + start) > extra_args.count
          puts "Command #{clean_cmd(cmd_id)} requires a minimum of #{part + start} extra arg#{(part + start) == 1 ? '' : 's'}".format_fg_red
          exit 1
        else
          args = get_vars([cmd.command, *cmd.args], extra_args)
          StateStore.looping = true
          cnt  = (args.count.to_f / part.to_f).ceil
          begin
            cur_args = args.slice!(0, part)
            exec_cmd(cmd_id, *cur_args)
            cnt2 = cnt - (args.count.to_f / part.to_f).ceil
            puts "\n#{" #{cnt2} of #{cnt} done (#{'%.2f' % ((cnt2.to_f / cnt.to_f) * 100)}%) ".center(40, '=')}\n\n"
          end until args.nil? || args.empty?
          StateStore.looping = false
        end
      else
        blank_args    = Array.new(start, '')
        cmd_with_vars = subs_vars([cmd.command], [*blank_args, *extra_args], true).first.first
        cmd_to_use, cmd_args = subs_command(cmd_with_vars, cmd.args)
        print_cmd(cmd.sudo, cmd_to_use, cmd_args, [*blank_args, *extra_args], cmd.interactive) unless cmd.silent
        run_cmd(cmd.sudo, cmd.interactive, cmd_to_use, cmd_args, [*blank_args, *extra_args])
      end
    end
  else
    puts "Could not find command #{cmd_id.to_s}".format_fg_yellow
  end
}