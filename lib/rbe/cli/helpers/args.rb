require 'shellwords'

global.helpers[:array_to_args] =->(cmd_arr, arr, prompt_if_missing_required = false) {
  literal = options[:literal]
  unescape_patterns = [[/\\*\+/, '+'],[/\\*[{]\\*[{]\\*([#])?/, '{{\1'], [/\\*[}]\\*[}]/, '}}'], [/\\*[*]/, '*']]
  (literal ? [*cmd_arr, *arr] : subs_vars(cmd_arr, arr, prompt_if_missing_required).first).flat_map { |v| Rbe::Data::DataStore.subs[v] || v }.map { |v|
    v.match?(/^(\||\d?>|<|\$\(|;|[&]{1,2}$|'.*'$|\[\[.*\]\]$|!!)/) ? (v.start_with?('!!') ? v[2..-1] : v) : unescape_patterns.reduce(Shellwords.escape(v)) { |s, p| s.gsub(p[0], p[1]) }
  }
}

global.helpers[:extract_args] =->(cmd_id, *args) {
  arr = []
  found = true
  args.each { |v|
    if v.start_with?('+')
      cur_id = v[1..-1]
      found  = cur_id == cmd_id || cur_id.start_with?("#{cmd_id}+")
      arr << cur_id[(cmd_id.length)..-1] if cur_id.start_with?("#{cmd_id}+")
    elsif found
      arr << v
    end
  }
  arr
}

global.helpers[:make_interactive] =->(cmd, arr, interactive) {
  cmd_str = "#{cmd} #{arr.join(' ')}"
  if interactive
    "#{interactive} --login -i -c #{Shellwords.escape(cmd_str)}"
  else
    cmd_str
  end
}

global.helpers[:subs_command] =->(cmd, cmd_args = []) {
  subs = Rbe::Data::DataStore.subs[cmd].clone || cmd
  if subs.is_a?(Array)
    ncmd = subs.shift
    [ncmd, [*subs, *cmd_args]]
  else
    [subs, cmd_args]
  end
}