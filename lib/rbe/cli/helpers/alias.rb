global.helpers[:update_rbe_reload] =->(prefix) {
  IO.write(File.expand_path('~/rbe_reload.sh'),
<<-EOS
#/usr/bin/env bash
function rbe_reload {
    #{prefix}
    echo "reloading rbe shell integration"
    rbe alias update
    source ~/rbe_reload.sh
    source ~/rbe_aliases.sh
}

function rbe {
    if [[ "$1" == "reload" ]]; then
        rbe_reload
    else
        command rbe $@
        if [[ $? -eq 11 ]]; then
          rbe_reload
          return 0
        fi
    fi
}
EOS
  )
}

global.helpers[:update_rbe_aliases] =->(prefix) {
  list = Rbe::Data::DataStore.aliases.list
  str  = <<-EOS
#/usr/bin/env bash
function rvsu {
    #{prefix}
    rbe s --rvm-sudo $@
}
function rsu {
    #{prefix}
    rbe s $@
}
function rce {
    #{prefix}
    rbe c e $@
}
function rta {
    #{prefix}
    rbe test-auth $@
}
function rccs {
    #{prefix}
    rbe c cmd-sort
}
function rvvs {
    #{prefix}
    rbe var var-sort
}
function rvls {
    #{prefix}
    rbe var list-sort $@
}
function raa {
    #{prefix}
    rbe alias add $@
    rbe_reload
}
EOS
  list.each { |li|
    str << <<-EOS
function #{li.to_s} {
    rce #{li.to_s} $@
}
EOS
  }
  IO.write(File.expand_path('~/rbe_aliases.sh'), str)
}

global.helpers[:update_bashrc] =-> {
  path = File.expand_path('~/.bashrc')
  if File.exist?(path)
    load_line = '[[ -s "$HOME/rbe_reload.sh" ]] && source "$HOME/rbe_reload.sh" && [[ -s "$HOME/rbe_aliases.sh" ]] && source "$HOME/rbe_aliases.sh" # Load rbe alias functions'
    lines     = IO.readlines(path).map(&:chomp)
    ind       = -1
    lines.each_with_index { |l, i|
      if l.to_s.include? 'rbe_reload.sh'
        ind = i
        break
      end
    }
    if ind >= 0
      lines[ind] = load_line
    else
      lines << load_line
    end

    IO.write(path, lines.join("\n"))
  end
}

global.helpers[:update_zshrc] =-> {
  path = File.expand_path('~/.zshrc')
  if File.exist?(path)
    load_line = '[[ -s "$HOME/rbe_reload.sh" ]] && source "$HOME/rbe_reload.sh" && [[ -s "$HOME/rbe_aliases.sh" ]] && source "$HOME/rbe_aliases.sh" # Load rbe alias functions'
    lines     = IO.readlines(File.expand_path('~/.zshrc')).map(&:chomp)
    ind       = -1
    lines.each_with_index { |l, i|
      if l.to_s.include? 'rbe_reload.sh'
        ind = i
        break
      end
    }
    if ind >= 0
      lines[ind] = load_line
    else
      lines << load_line
    end

    IO.write(path, lines.join("\n"))
  end
}