global.helpers[:register_temp_vars] =->(vars) {
  vars.keys.each { |k| Rbe::Data::DataStore.temp_vars[k.to_s] = vars[k] } if vars
}

global.helpers[:subs_vars] =->(cmd_arr, arr, prompt_if_missing_required = false) {
  ind_to_remove = []
  cmd_arr2      = cmd_arr.flat_map { |v|
    if v.nil?
      puts cmd_arr.inspect
    end
    if v =~ /^\*\*(.+)\*\*$/
      v2 = $1
      loop_var = true
    end
    start = 0
    rv = []
    loop {
      mx = -1
      rv << (v2 || v).gsub(%r{[{][{]([#]?[\w\d\-\+]+)(?:[=][>](\d+))?(?:~/(.+)/[?][{](.*)[}][:][{](.*)[}])?[}][}]}) { |_|
        mx = [mx, $2.to_i].max if $2
        ind = $2 && ($2.to_i + start)
        ind_to_remove << ind if arr.count > 0 && ind && arr[ind]
        v3 = Rbe::Data::DataStore.var($1, prompt_if_missing_required, arr.count > 0 && ind && arr[ind.to_i])
        if $3
          t = $4
          f = $5
          if (v3 || '') =~ /#{$3}/
            t
          else
            f
          end
        else
          v3
        end
      }
      start += mx + 1
      break unless loop_var && arr[start]
    }
    rv.flat_map { |i|
      if i =~ %r{^[{][{][*]([\w\d]+)[}][}]$}
        Rbe::Data::DataStore.vars[$1] || i
      else
        i
      end
    }
  }
  arr2          = [].replace(arr)
  ind_to_remove.uniq.sort.reverse_each { |itr| arr2.delete_at(itr) }
  [[*cmd_arr2, *arr2], cmd_arr2, arr2]
}

global.helpers[:get_vars] =->(cmd_arr, arr) {
  ind_to_remove = []
  cmd_arr.each { |v|
    v.scan(%r{[{][{]([#]?[\w\d]+)(?:[=][>](\d+))?(?:~/(.+)/[?][{](.*)[}][:][{](.*)[}])?[}][}]}) { |vname, ind|
      unless vname == '_' || vname == '#_'
        ind_to_remove << ind.to_i if arr.count > 0 && ind && arr[ind.to_i]
        v = Rbe::Data::DataStore.var(vname, true, arr.count > 0 && ind && arr[ind.to_i])
        if $3
          t = $4
          f = $5
          if v =~ /#{$3}/
            t
          else
            f
          end
        else
          v
        end
      end
    }
  }
  arr2 = [].replace(arr)
  ind_to_remove.uniq.sort.reverse_each { |itr| arr2.delete_at(itr) }
  arr2
}

global.helpers[:count_ind_vars] =->(cmd, cmd_arr) {
  inds = [cmd, *cmd_arr].map { |v|
    v.scan(%r{[{][{][#]?_(?:[=][>](\d+))(?:~/(.+)/[?][{](.*)[}][:][{](.*)[}])?[}][}]}).map { |v2|
      v2[0].to_i
    }
  }.flatten.uniq
  inds = [0] if inds.empty?
  min  = inds.min
  max  = inds.max
  [min, max + 1 - min]
}