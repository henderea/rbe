def load_keychain
  unless @keychain_loaded
    @keychain_loaded = true
    begin
      require 'keychain'
      @supports_keychain = true
    rescue LoadError
      @supports_keychain = false
    end
  end
  @supports_keychain
end

module Rbe::Data
  class Password
    class << self
      def user
        `whoami`.chomp
      end

      def get
        if load_keychain
          kc = Keychain.generic_passwords.where(service: 'rbe').first
          kc && kc.password
        else
          false
        end
      end

      def set(password)
        if load_keychain
          kc = Keychain.generic_passwords.where(service: 'rbe').first
          if kc.nil?
            Keychain.generic_passwords.create(service: 'rbe', password: password, account: self.user)
          else
            kc.password = password
            kc.save!
          end
          self.get.nil? ? :failure : :success
        else
          false
        end
      end

      def delete
        if load_keychain
          kc = Keychain.generic_passwords.where(service: 'rbe').first
          if kc.nil?
            :nil
          else
            kc.delete
            kc = Keychain.generic_passwords.where(service: 'rbe').first
            kc.nil? ? :success : :failure
          end
        else
          false
        end
      end
    end
  end
end