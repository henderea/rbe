require 'everyday_natsort'
require_relative 'data_loader'

module Rbe::Data
  class AbstractGlobalList
    class << self
      def abstract(*names)
        names.each { |name|
          define_method(name.to_sym) { |*args| raise "Method #{__method__}(#{args.join(', ')}) not implemented." }
        }
      end
    end

    abstract :file_name
    abstract :list
    abstract :list=
    abstract :has_key?
    abstract :keys
    abstract :[]
    abstract :[]=
    abstract :delete

    def on_init
      #do nothing
    end

    def initialize
      load_list
      on_init
    end

    def sort_list
      self.list = Hash[EverydayNatsort.sort(self.list)]
      save_list
    end

    def load_list
      self.list = Rbe::Data::DataLoader.load_file("~/#{self.file_name}", {}, [:json, :yaml], force_type: :json)
    end

    def save_list
      Rbe::Data::DataLoader.save_file("~/#{self.file_name}", :json, self.list) unless @no_save
    end

    def no_save
      @no_save = true
      yield
      @no_save = false
    end

    # protected :save_list, :save_local_list, :load_list, :load_local_list, :list, :local_list, :list=, :local_list=, :on_init
  end
end