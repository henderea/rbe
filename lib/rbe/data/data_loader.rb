module Rbe::Data
  class DataLoader
    class << self
      def load_file(base_name, default_value, types, options = {})
        rval = default_value
        matched_type = :none
        types.each { |type|
          if type == :json
            filename = File.expand_path("#{base_name}.json")
            if File.exist?(filename)
              require 'json'
              rval = JSON.parse(IO.read(filename))
              matched_type = :json
              break
            end
          elsif type == :yaml
            filename = File.expand_path("#{base_name}.yaml")
            if File.exist?(filename)
              require 'yaml'
              rval = YAML.load_file(filename)
              matched_type = :yaml
              break
            end
          end
        }
        if options[:force_type] && [:json, :yaml].include?(options[:force_type]) && matched_type != :none && matched_type != options[:force_type]
          save_file(base_name, options[:force_type], rval)
        end
        rval
      end

      def save_file(base_name, type, data)
        if type == :json
          require 'json'
          IO.write(File.expand_path("#{base_name}.json"), JSON.pretty_generate(data))
        elsif type == :yaml
          require 'yaml'
          IO.write(File.expand_path("#{base_name}.yaml"), data.to_yaml)
        end
      end
    end
  end
end