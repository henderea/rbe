require 'readline'
require_relative 'abstract_global_list'

module Rbe::Data
  class SubsList < Rbe::Data::AbstractGlobalList
    def list
      @subs
    end

    def list=(list)
      @subs = list
    end

    def file_name
      'subs.rbe'
    end

    def write_subs
      save_list unless @subs.empty?
    end

    def [](sub_name)
      @subs[sub_name]
    end

    def []=(sub_name, value)
      @subs[sub_name] = value
      save_list
    end

    def has_key?(sub_name)
      @subs.has_key?(sub_name)
    end

    def keys
      @subs.keys
    end

    def delete(sub_name)
      @subs.delete(sub_name)
      save_list
    end

    # protected :list, :local_list, :list=, :local_list=, :on_init, :get_default, :get_required_prompt, :get_required
  end
end